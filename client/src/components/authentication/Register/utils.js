const serverUrl = 'http://localhost:9000'

const nameValidation = { required: true, maxLength: 20, pattern: /^[A-Za-z\u0590-\u05fe]+$/i, }
export const validation = {
    firstName: nameValidation,
    lastName: nameValidation,
    password: { required: true, pattern: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/ },
    email: { required: true, pattern: /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/ },
}

export const registertion = params => {
    const url = new URL(`${serverUrl}/register`)
    url.search = new URLSearchParams(params).toString()
    return fetch(url)
}
