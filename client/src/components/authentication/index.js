import React, { useState } from 'react'
import { Card } from '@mui/material'
import Login from './Login'
import Register from './Register'
import './autentication.css'

const Authentication = () => {
  const [haveAccount, setHaveAccount] = useState(true)
  return (
    <Card className='card'>
      { haveAccount ? <Login toRegister={() => setHaveAccount(false)} /> : <Register toLogin={() => setHaveAccount(true)} /> }
    </Card>
  )
}

export default Authentication