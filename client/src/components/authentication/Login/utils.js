import { getHeaders } from "../../../utils";

const serverUrl = 'http://localhost:9000'

export const login = async params => {
    const url = new URL(`${serverUrl}/login`)
    url.search = new URLSearchParams(params).toString();
    const res = await fetch(url, { headers: getHeaders() })
    
    if (res.status !== 200) throw res

    const { firstName, lastName, token, type } = await res.json()
    window.localStorage.firstName = firstName
    window.localStorage.lastName = lastName
    window.localStorage.token = token
    window.localStorage.type = type
}
