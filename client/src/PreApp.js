import React, { useEffect, useState } from 'react'
import './App.css'
import LoadingPage from './LoadingPage'
import App from './App'
import Login from './Login/Login'

const reqponseToPage = {
    'loading': <LoadingPage />,
    'app': <App />,
    // 'err': <Err />,
    'login': <Login />
}

function PreApp() {
  const [page, setPage] = useState(reqponseToPage['loading'])

  useEffect(() => {
    const init = async () => {
        let page
        try {
            const { data } = await fetch('/api')
            console.log(data)
            page = reqponseToPage['app']
        } catch (err) {
            page = reqponseToPage['err']
        }

        if (page)
          setPage(page)
    }
    setTimeout(init, 1000)
  }, [])

  return page
}

export default PreApp
