const { MongoClient } = require('mongodb')

const dbUri = process.env.DB_URI
const dbName = process.env.DB_NAME

let dbConnection

const dbConnect = async () => {
    if (!dbConnection) {
        try {
            const client = new MongoClient(dbUri)
            await client.connect()
            dbConnection = client.db(dbName)
        } catch(e) {
            console.log(e)
        }    
    }
    return dbConnection
}

module.exports = { dbConnect }