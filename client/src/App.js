import React, { useEffect, useState } from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Navbar from './components/Navbar'
import HomePage from './components/HomePage'
import Authentication from './components/authentication'
import { login } from './components/authentication/Login/utils'
import { routes } from './consts'

const App = () => {
    const [isConnected, setIsConnected] = useState(false)
    useEffect(() => {
        const checkConnection = async () => {
            try {
                await login()
                setIsConnected(true)
            } catch (error) {
                setIsConnected(false)
            }
        }
        checkConnection()
    }, [])

    return <>
        <Navbar isConnected={isConnected}/>
        <div style={{ position: 'absolute', width: '100vw', backgroundImage: `url(${backgroundImage})` }}>
            <Routes>
                <Route path={routes.home} element={<HomePage />} />
                <Route path={routes.authentication} element={<Authentication />} />
            </Routes>
        </div>
    </>
}

const backgroundImage = `${process.env.PUBLIC_URL}/images/login-background.svg`

const AppWrap = () => {
    return <Router><App/></Router>
} 

export default AppWrap