export const resetUserData = () => {
    window.localStorage.token = ''
    window.localStorage.firstName = ''
    window.localStorage.lastName = ''
    window.location.reload()
}