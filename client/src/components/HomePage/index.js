import React from 'react'
import { Button } from '@mui/material'
import './homePage.css'

const buttonCustomStyle = {
  marginTop: '2vh',
  fontSize: '1.5em',
  backgroundColor: 'white',
  color: '#2d80c0',
  '&:hover': {
    backgroundColor: '#eeeeee',
  },
}

const organizations = ['משרד התחבורה', 'משרד הבריאות', 'משרד הביטחון', 'אגף החינוך', 'משרד התרבות והספורט', 'רשויות מקומיות']
const HomePage = () => {
  return (
    <div className='homePage' >
        <h1>חדשים באתר</h1>
        <br/>
        <div className='cards'>
          {organizations.map(organization => <Button variant="contained" sx={buttonCustomStyle}>{organization}</Button>)}
        </div>
    </div>
  )
}

export default HomePage