const jwt = require('jsonwebtoken'),
    { ObjectId } = require('mongodb')

const { dbConnect } = require('../utils/dbConnect')

const collection = 'users'
const isAuthenticationRoute = path => path.includes('login') || path.includes('register')
const authenticationMiddleware = async (req, res, next) => {
    try {
        if (isAuthenticationRoute(req.path))
            return next()

        const { token } = req.headers
        if (!token) {
            const err = new Error('User is not connected')
            err.code = 401
            throw err
        }
        let tokenData
        try {
            tokenData = jwt.verify(token, process.env.TOKEN_KEY)
        } catch (error) {
            const err = new Error('עבר מעל לשעתיים מההתחברות האחרונה למערכת, נא להתחבר מחדש')
            err.code = 401
            throw err
        }

        const { _id, email, password } = tokenData
        const db = await dbConnect()
        const user = await db.collection(collection).findOne({ _id: ObjectId(_id), email, password })
        
        if (!user) {
            const err = new Error('User not found, please Login')
            err.code = 401
            throw err
        }
        
        req.user = user
        next()
    } catch (err) {
        return next(err)
    }
}

module.exports = authenticationMiddleware