const express = require("express")

const router = express.Router()

router.get('/api', async (req, res) => {
    res.send('app')
})

module.exports = router;