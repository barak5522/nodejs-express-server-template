import React, { useState } from 'react'
import { useForm } from "react-hook-form"
import { InputAdornment, OutlinedInput, Button, IconButton } from '@mui/material'
import { Visibility, VisibilityOff } from '@mui/icons-material'

import { login } from './utils'
import { routes } from '../../../consts'
import './login.css'

const Login = ({ toRegister }) => {
  const [showPassword, setShowPassword] = useState(false)
  const { register, handleSubmit } = useForm()
  const onSubmit = async data => {
    try {
      await login(data).then(() => {
        window.location.replace(routes.home)
      })
    } catch (error) {
      window.alert(error.statusText || error.massage)
    }
  }
   
  return (
    <>
      <h1 className='center'>Welcome</h1>
      <form className='form' onSubmit={handleSubmit(onSubmit)}>
        <OutlinedInput placeholder='Email address' {...register("email")} />
        <OutlinedInput placeholder='Password' {...register("password")} type={showPassword ? 'text' : 'password' } endAdornment={
          <InputAdornment>
              <IconButton onClick={() => setShowPassword(showPassword => !showPassword)}>
                  { showPassword ? <VisibilityOff /> : <Visibility /> }
            </IconButton>
          </InputAdornment>
        } />
        <Button className='center'>Forgot password?</Button>
        <Button variant="contained" onClick={handleSubmit(onSubmit)}>Login</Button>
        <span className='center'>Not a member?<Button onClick={toRegister}>Register</Button></span>
      </form>
    </>
  )
}

export default Login