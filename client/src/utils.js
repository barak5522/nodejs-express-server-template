export const getHeaders = () => ({ 'Content-Type': 'application/json', token: window.localStorage.token })

export const handleFetch = async (url, callback) => {
    try {
      const res = await fetch(url, { headers: getHeaders() })
      if (res.status === 401) {
        const error = new Error(res.statusText)
        error.status = 401
        throw error
      }
      const data = await res.json()
      callback(data)
    } catch(error) {
      window.alert(error.message || 'התרחשה תקלה')
    }
  }