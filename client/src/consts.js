export const routes = {
    home: '/',
    auctions: '/auctions',
    authentication: '/authentication'
}