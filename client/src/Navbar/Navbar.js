import React from 'react'

import './Navbar.css'

function Navbar({ title }) {
  return (
    <div className="body">
        {title}
    </div>
  )
}

export default Navbar
