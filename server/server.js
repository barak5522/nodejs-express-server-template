const cors  = require('cors'),
    express = require('express'),
    authentication = require('./routes/authentication'),
    authenticationMiddleware = require('./middlewares/authentication'),
    errorHandler = require('./middlewares/errorHandler')


const PORT = process.env.PORT || 9000

app = express()

app.use(express.json())
app.use(cors())

// custom pre middlewares
app.use(authenticationMiddleware)

// routes
app.use(authentication)

// custom post middleware
app.use(errorHandler)

app.listen(PORT, () => {
    console.log(`Listening on port: ${PORT}`);
})
