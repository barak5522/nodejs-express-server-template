const errorHandler = (err, req, res, next) => {
    const code = err.code
    const message = err.message;
    res.writeHead(code, message, {'content-type' : 'text/plain'});
    res.end(message);
}

module.exports = errorHandler