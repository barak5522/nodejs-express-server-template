import React, { useRef, useState } from "react"
import { NavLink } from "react-router-dom"
import { Person as PersonIcon, Search as SearchIcon, Home as HomeIcon, AttachFile as AttachFileIcon } from '@mui/icons-material'
import { IconButton, Popper, Grow, ClickAwayListener, Paper, MenuList, MenuItem } from '@mui/material'
import { resetUserData } from "./utils"
import { routes } from '../../consts'
import "./Navbar.css"

const Navbar = ({ isConnected }) => {
    const anchorRef = useRef(null)
    const [open, setOpen] = useState(false)

    const handleToggle = () => {
        setOpen(prevOpen => !prevOpen)
    }
    const handleClose = event => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) return
        setOpen(false)
    }

    return <div className="navbar">
        <div className="menu">
            <div>
                <IconButton
                    ref={anchorRef}
                    id="composition-button"
                    onClick={handleToggle}
                    >
                    <PersonIcon fontSize="large" className="material-icons"/>
                </IconButton>
                { `שלום ${(isConnected && window.localStorage.firstName) || ''}` }
                <Popper
                    open={open}
                    anchorEl={anchorRef.current}
                    transition
                    disablePortal
                    style={{  zIndex: 1}}
                    >
                    {({ TransitionProps }) => (
                    <Grow
                        {...TransitionProps}
                        style={{  zIndex: 1, transformOrigin: 'left top' }}
                        >
                        <Paper>
                            <ClickAwayListener onClickAway={handleClose}>
                            <MenuList autoFocusItem={open}>
                                { isConnected ?
                                    <MenuItem onClick={resetUserData}>התנתק</MenuItem> :
                                    <MenuItem><NavLink to={routes.authentication}>התחבר</NavLink></MenuItem>
                                }
                            </MenuList>
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
                </Popper>
            </div>
            <hr/>
            <div>
                <NavLink to={routes.auctions}>
                    <IconButton>
                        <SearchIcon fontSize="large" className="material-icons"/>
                    </IconButton>
                    חיפוש
                </NavLink>
            </div>
            <hr/>
            <div>
                <NavLink to={routes.home}>
                    <IconButton>
                        <HomeIcon fontSize="large" className="material-icons"/>
                    </IconButton>
                    מסך בית
                </NavLink>
            </div>
            <hr/>
            <div>
                <AttachFileIcon fontSize="large" className="material-icons"/>
                <span>
                    שמורים
                </span>
            </div>
        </div>
    </div>
}

export default Navbar