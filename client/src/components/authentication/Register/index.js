import React, {useState} from 'react'
import { useForm } from 'react-hook-form'
import { OutlinedInput, Button, RadioGroup, FormControlLabel, Radio } from '@mui/material'

import { registertion, validation } from './utils'
import './register.css'

const Register = ({ toLogin }) => {
  const { register, formState: { errors }, handleSubmit } = useForm()
  const [userType, setUserType] = useState('supplier')
  const onSubmit = async data => {
    try {
      const res = await registertion({...data, type: userType})
      if (res.status !== 200) throw res

      window.alert('נרשמת בהצלחה, נא להתחבר למערכת')
      toLogin()
    } catch (error) {
      window.alert(error.statusText || error.massage)
    }
  }
   
  return (
    <>
      <h1 className='center'>Create an account</h1>
      <form className='form' onSubmit={handleSubmit(onSubmit)}>
        <OutlinedInput placeholder='First name' {...register('firstName', validation.firstName)} />
        {errors.firstName && <p role='alert'>invalid first name</p> }
        <OutlinedInput placeholder='Last name' {...register('lastName', validation.lastName)} />
        {errors.lastName && <p role='alert'>invalid last name</p> }
        <OutlinedInput placeholder='Email address' {...register('email', validation.email)} />
        {errors.email && <p role='alert'>invalid email</p> }
        <OutlinedInput placeholder='Password' {...register('password', validation.password)} />
        {errors.password && <p role='alert'>invalid password</p> }
        <RadioGroup value={userType} onChange={({target: {value}}) => setUserType(value)} className='types'>
          <FormControlLabel value='supplier' control={<Radio />} label='ספק' />
          <FormControlLabel value='customer' control={<Radio />} label='לקוח' />
        </RadioGroup>
        <Button variant='contained' onClick={handleSubmit(onSubmit)}>Register</Button>
        <span className='center'>already have an account?<Button onClick={toLogin}>Login</Button></span>
      </form>
    </>
  )
}

export default Register