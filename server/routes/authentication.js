const express = require('express'),
      jwt = require('jsonwebtoken'),
      { ObjectId } = require('mongodb')

const { dbConnect } = require('../utils/dbConnect')
const router = express.Router()

const collection = 'users'
const tokenKey = process.env.TOKEN_KEY

router.get('/register', async (req, res, next) => {
    try {
        const { email } = req.query
        const db = await dbConnect()
        const user = await db.collection(collection).findOne({ email })
        if (user) {
            const err = new Error('User already exist')
            err.code = 400
            throw err
        }
        await db.collection(collection).insertOne(req.query)

        res.send()
        return next()
    } catch (err) {
        return next(err)
    }
})

const loginWithToken = async token => {
    try {
        if (token) {
            const { _id, email, password } = jwt.verify(token, tokenKey)
            const db = await dbConnect()
            const user = await db.collection(collection).findOne({ _id: ObjectId(_id), email, password })
            return user
        } else {
            throw new Error()
        }
    } catch (e) {
        return null
    }
}

router.get('/login', async (req, res, next) => {
    try {
        let user, token
        token = req.headers.token 
        user = await loginWithToken(token)

        if (!user) {
            const { password, email } = req.query
            const db = await dbConnect()
            user = await db.collection(collection).findOne({ email })
            if (!user) {
                const err = new Error('User does not exist')
                err.code = 401
                throw err
            }
            if (user.password !== password) {
                const err = new Error('Wrong password')
                err.code = 401
                throw err
            }

            token = jwt.sign({ _id: user._id, email: user.email, password: user.password }, tokenKey, { expiresIn: '2h' })
        }    
        
        const userData = {
            token,
            type: user.type,
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
        }
      
        res.status(200).json(userData)
        return next()
    } catch (err) {
        return next(err)
    }
})

module.exports = router
