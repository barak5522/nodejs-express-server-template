import React from 'react'

import './App.css';

const LoadingPage = () =>
    <div className='loading-page'>
        <img src={'images/loading.gif'} alt='העמוד טוען, מייד יעלה'/>
    </div>

export default LoadingPage